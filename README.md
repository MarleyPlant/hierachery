<h1><b>Hierachery</b> - React app for ranking your priorities in life. </h1>




![Gitlab Pipeline Status](https://img.shields.io/gitlab/pipeline-status/MarleyPlant/hierachery?style=flat-square) ![GitLab Language Count](https://img.shields.io/gitlab/languages/count/MarleyPlant/hierachery?style=flat) <br />

<br />


![Demo Gif](./.dev/demo.gif)

<br />


## 💻 **TECHNOLOGIES**
[![React](https:&#x2F;&#x2F;img.shields.io&#x2F;badge&#x2F;React-61DAFB?style&#x3D;for-the-badge&amp;logo&#x3D;React&amp;logoColor&#x3D;white)]()
[![CSS3](https:&#x2F;&#x2F;img.shields.io&#x2F;badge&#x2F;CSS3-1572B6?style&#x3D;for-the-badge&amp;logo&#x3D;CSS3&amp;logoColor&#x3D;white)]()
[![HTML5](https:&#x2F;&#x2F;img.shields.io&#x2F;badge&#x2F;HTML5-E34F26?style&#x3D;for-the-badge&amp;logo&#x3D;HTML5&amp;logoColor&#x3D;white)]()
[![npm](https:&#x2F;&#x2F;img.shields.io&#x2F;badge&#x2F;npm-CB3837?style&#x3D;for-the-badge&amp;logo&#x3D;npm&amp;logoColor&#x3D;white)]()
[![TypeScript](https:&#x2F;&#x2F;img.shields.io&#x2F;badge&#x2F;TypeScript-3178C6?style&#x3D;for-the-badge&amp;logo&#x3D;TypeScript&amp;logoColor&#x3D;white)]()
[![Sass](https:&#x2F;&#x2F;img.shields.io&#x2F;badge&#x2F;Sass-CC6699?style&#x3D;for-the-badge&amp;logo&#x3D;Sass&amp;logoColor&#x3D;white)]()
[![JavaScript](https:&#x2F;&#x2F;img.shields.io&#x2F;badge&#x2F;JavaScript-F7DF1E?style&#x3D;for-the-badge&amp;logo&#x3D;JavaScript&amp;logoColor&#x3D;white)]()

<br />


## 👨‍💻 Development

### 🔌 Installing Dependencies
```bash
npm install
```

### 🟢 Running The Project
```bash
npm start
```

### 🧪 Testing
```bash
npm test
```

<br />


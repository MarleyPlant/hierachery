import React from "react";
import $ from "jquery";
import { tier } from "./interfaces";
import {
  ContainerCenter,
  FontLoader,
  SelectorsGrid,
  NotEnoughSelectionsDialog,
} from "./components/index";

import { isEmpty } from "./helpers/";

import { CompareModal } from "./components/CompareModal";

import {
  Button,
  Grid,
  TextField,
  InputAdornment,
  IconButton,
} from "@material-ui/core";
import { GitHub, PlusOne } from "@material-ui/icons/";
import { ThemeProvider } from "@material-ui/core/styles";
import { theme } from "./constants/";
import "./scss/app.scss";

//create your forceUpdate hook
function useForceUpdate() {
  const [value, setValue] = React.useState(0); // integer state
  return () => setValue(value + 1); // update the state to force render
}

function App() {
  const [theSelectors, setTheSelectors] = React.useState<tier[]>([]);
  const [showIcons, setshowIcons] = React.useState<boolean>(true);
  const [compareModalOpen, setCompareModalOpen] =
    React.useState<boolean>(false);
  const [NotEnoughSelectionsDialogOpen, setNotEnoughSelectionsDialogOpen] =
    React.useState<boolean>(false);
  let newRef = React.useRef<HTMLDivElement[]>([]);

  const forceUpdate = useForceUpdate();

  const compareModalHandleOpen = () => {
    if (theSelectors.length <= 2) {
      setNotEnoughSelectionsDialogOpen(true);
      return;
    } else {
      setCompareModalOpen(true);
    }
  };

  const compareModalHandleClose = () => {
    setCompareModalOpen(false);
  };

  function addSelectorChange(val: any) {

    if (!val.target[0]) {
      val.target = document.getElementById("addSelectorForm");
    }

    const selectorAlreadyExists = theSelectors.find(
      (selector) =>
        selector.text.toUpperCase() === val.target[0].value.toUpperCase()
    );

    val.preventDefault();

    if (selectorAlreadyExists) {
      val.target.reset();
      return;
    }

    if (theSelectors.length >= 9) {
      setCompareModalOpen(true);
      val.target[0].disabled = true; //Disable Input when full
    }

    if (isEmpty(val.target[0].value)) {
      val.target.reset();
      return val;
    }

    addSelector(setTheSelectors, theSelectors, val);
    val.target.reset();
  }

  function handleDeleteSelector(props: any) {
    let clickedSelectorText = $(
      $(props.target).parent().first()[0]["firstChild"]
    )[0]["innerHTML"];
    let found = findSelector(theSelectors, clickedSelectorText);

    if (found >= 0) {
      let returner = theSelectors;
      if (found === returner.length - 1) {
        returner.pop();
      } else if (found === 0) {
        returner.shift();
      } else {
        returner.splice(found, found);
      }

      setTheSelectors(returner);
      forceUpdate();
    }
  }

  return (
    <ThemeProvider theme={theme}>
      <FontLoader></FontLoader>
      <ContainerCenter>
        <div className="app-heading">
          <img
            width="50em"
            height="50em"
            alt="logo"
            src={process.env.PUBLIC_URL + "/logo.png"}
          ></img>
          <h1 className="addpage-title">Hierarchery</h1>
        </div>
        <p className="addpage-subtitle">
          Welcome to Hierarchery, if you are struggling to Prioritise your list,
          this #1 app can help you in a few quick and easy steps
          <br /> <br /> Simply Enter Between 4 and 10 Selections below.
        </p>
        <form
          style={{ minWidth: "80%" }}
          id="addSelectorForm"
          onSubmit={addSelectorChange}
        >
          <TextField
            autoComplete="off"
            id="outlined-helperText"
            label={`Selection ${theSelectors.length + 1}`}
            defaultValue=""
            helperText=""
            variant="outlined"
            fullWidth
            onFocus={() => {
              setshowIcons(false);
            }}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <IconButton
                    edge="end"
                    color="primary"
                    onClick={addSelectorChange}
                  >
                    <PlusOne />
                  </IconButton>
                </InputAdornment>
              ),
            }}
          />
        </form>
        <br />

        <Grid
          className="selectors"
          container
          spacing={2}
          direction="row"
          justifyContent="center"
          alignItems="center"
        >
          <SelectorsGrid
            newRef={newRef}
            theSelectors={theSelectors}
            handleDeleteSelector={handleDeleteSelector}
          />
        </Grid>

        <Button
          size="large"
          variant="outlined"
          onClick={compareModalHandleOpen}
          color={theSelectors.length > 2 ? "primary" : "secondary"}
        >
          RANK {theSelectors.length} SELECTIONS
        </Button>

        <div
          style={{ display: showIcons ? "block" : "none" }}
          className="social-icons"
        >
          <a href="https://gitlab.com/MarleyPlant/Hierarchery">
            <GitHub></GitHub>
          </a>
        </div>
      </ContainerCenter>

      <CompareModal
        theSelectors={theSelectors}
        setTheSelectors={setTheSelectors}
        open={compareModalOpen}
        handleClose={compareModalHandleClose}
      />
      <NotEnoughSelectionsDialog
        set={setNotEnoughSelectionsDialogOpen}
        open={NotEnoughSelectionsDialogOpen}
      />
    </ThemeProvider>
  );
}

export default App;
function findSelector(theSelectors: tier[], clickedSelectorText: any) {
  return theSelectors.findIndex(
    (selector) => selector.text === clickedSelectorText
  );
}

function addSelector(
  setTheSelectors: React.Dispatch<React.SetStateAction<tier[]>>,
  theSelectors: tier[],
  val: any
) {
  setTheSelectors([
    ...theSelectors,
    {
      text: val.target[0].value.toUpperCase(),
      points: 0,
    },
  ]);
}

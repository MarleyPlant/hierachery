import { tier, selectorReference } from "../interfaces";
import { useState } from "react";

export function useForceUpdate() {
  const [value, setValue] = useState(0); // integer state
  return () => setValue(() => value + 1); // update the state to force render
}

export function sortSelectors(selectors: tier[]) {
  return selectors.sort((a, b) => b.points - a.points).slice(0, 5);
}

export function isEmpty(value: any) {
  return value === "" || value === null || value === " ";
}

export const findTopThree = (theSelectors: any, setTopThree: any) => {
  let returner = sortSelectors(theSelectors);
  let val = [returner[0], returner[1], returner[2]];

  setTopThree(val);
};

export function updateCurrent(
  setCurrent: React.Dispatch<React.SetStateAction<selectorReference>>,
  index: number,
  theSelectors: tier[]
) {

  setCurrent({
    index: index,
    object: theSelectors[index],
  });
}

export function addPoint(
  selectedIndex: number,
  theSelectors: any,
  setTheSelectors: React.Dispatch<React.SetStateAction<tier[]>>
) {
  theSelectors[selectedIndex]["points"] += 1;
  setTheSelectors(theSelectors);
}


import { Chip } from "@material-ui/core";

export default function SelectorsGrid(props: any) {
  return (
    <>
      {props.theSelectors.map((selection: any, key: number) => {
        return (
          <div
            ref={(el) => {
              props.newRef.current[key] = el as HTMLDivElement;
            }}
            key={key}
            className="selector"
          >
            <Chip
              color="primary"
              onMouseDown={(event: any) => {
                event.stopPropagation();
              }}
              onDelete={props.handleDeleteSelector}
              label={selection.text}
              variant="outlined"
            />
          </div>
        );
      })}
    </>
  );
}

import React from "react";
import {
  Modal,
  Container,
  Button,
  CircularProgress,
  LinearProgress,
} from "@material-ui/core";
import { selectorReference, tier } from "../interfaces";
import { findTopThree, addPoint, updateCurrent } from "../helpers/";
export class CompareModal extends React.Component<
  {
    open: any;
    handleClose: any;
    theSelectors: tier[];
    setTheSelectors: any;
  },
  {
    completedComparisons: boolean;
    current: selectorReference;
    comparison: selectorReference;
    stepsCompleted: number;
    topThree: tier[];
  }
> {
  constructor(props: any) {
    super(props);
    this.state = {
      current: { index: 1, object: null },
      comparison: { index: 0, object: null },
      completedComparisons: false,
      stepsCompleted: 0,
      topThree: [],
    };

    this.modalNext = this.modalNext.bind(this);
    this.CompareModalContent = this.CompareModalContent.bind(this);
    this.setCurrent = this.setCurrent.bind(this);
    this.setComparison = this.setComparison.bind(this);
    this.setTopThree = this.setTopThree.bind(this);
    this.render = this.render.bind(this);
    this.exportData = this.exportData.bind(this);
    this.getTotalSteps = this.getTotalSteps.bind(this);
    this.CompareModalContentCompleted =
      this.CompareModalContentCompleted.bind(this);
  }

  componentDidUpdate() {
    if (this.props.open && !this.state.current.object) {
      updateCurrent(this.setCurrent, 1, this.props.theSelectors);
    }

    if (this.props.open && !this.state.comparison.object) {
      this.setComparison({
        index: 0,
        object: this.props.theSelectors[0],
      });
    }
  }

  render() {
    return (
      <Modal
        open={this.props.open}
        onClose={this.props.handleClose}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        <Container className="compareModal">
          {this.state.completedComparisons ? (
            <this.CompareModalContentCompleted topThree={this.state.topThree} />
          ) : (
            <this.CompareModalContent />
          )}
        </Container>
      </Modal>
    );
  }

  CompareModalContent(props: any) {
    return (
      <>
        <LinearProgress
          data-testid="progress"
          value={(this.state.stepsCompleted / this.getTotalSteps()) * 100}
          variant="determinate"
        ></LinearProgress>
        <div className="compareModalContent">
          <div className="compareModalHeadings">
            <h3>Prioritise Your List, Choose your personal priority</h3>
            <p>Compare Selections in your list, which is your priority?</p>
          </div>
          <div className="compareModalButtons">
            <Button
              data-testid="leftBtn"
              className="modalButton"
              onClick={this.modalNext}
              variant="outlined"
              color="primary"
            >
              {this.state.comparison.object
                ? this.state.comparison.object.text
                : "ERROR"}
            </Button>
            <h3 className="compareModalOR">OR</h3>
            <Button
              data-testid="rightBtn"
              className="modalButton"
              onClick={this.modalNext}
              variant="outlined"
              color="primary"
            >
              {this.state.current.object
                ? this.state.current.object.text
                : "ERROR"}
            </Button>
          </div>
        </div>
      </>
    );
  }

  CompareModalContentCompleted(props: any) {
    return (
      <div className="compareModalContentCompleted">
        {this.state.topThree.map((selector: any, i: any) => {
          return (
            <div className="compareContentResult">
              <h3>#{i + 1}</h3>
              <Container key={i} className="compareModalFinalResultContainer">
                <CircularProgress
                  color="primary"
                  variant="determinate"
                  value={(selector.points / this.getTotalSteps()) * 100}
                />
                <p className="compareModalFinalResult">
                  <b>{selector.text}</b> ({selector.points})
                </p>
              </Container>
            </div>
          );
        })}
      </div>
    );
  }

  setCurrent(toSet: any) {
    this.setState({
      current: toSet,
    });
  }

  setComparison(toSet: any) {
    this.setState({
      comparison: toSet,
    });
  }

  setTopThree(toSet: any) {
    this.setState({
      topThree: toSet,
    });
  }

  updateComparison(key: any) {
    this.setState({
      comparison: {
        index: key,
        object: this.props.theSelectors[key],
      },
    });
  }

  exportData() {
    let exporter = {
      topThree: this.state.topThree,
      totalComparisons: this.getTotalSteps(),
      totals: this.props.theSelectors,
    };
    console.log(JSON.stringify(exporter));
  }

  private getTotalSteps() {
    return (
      this.props.theSelectors.length * this.props.theSelectors.length -
      this.props.theSelectors.length
    );
  }

  modalNext(props: any) {
    let selectedIndex = this.props.theSelectors.findIndex(
      (selector: any) => selector.text === props.target["innerHTML"]
    );
    let { theSelectors, setTheSelectors } = this.props;
    let nextCurrentIndex = this.state.current.index + 1;
    let nextComparisonIndex = this.state.comparison.index + 1;
    const selectorExists =
      selectedIndex >= 0 && this.props.theSelectors[this.state.current.index];

    if (!selectorExists) return;

    const isComparisonsCompleted =
      nextComparisonIndex >= theSelectors.length - 1;
    const isCurrentCompleted = nextCurrentIndex >= theSelectors.length;
    const areSelectorsTheSame =
      this.props.theSelectors[selectedIndex] ===
      this.props.theSelectors[this.state.current.index];

    if (areSelectorsTheSame) {
      this.nextComparison(theSelectors);
      return;
    }

    this.setState({
      stepsCompleted: this.state.stepsCompleted + 1,
    });

    if (
      !(nextComparisonIndex === this.props.theSelectors.length) &&
      nextCurrentIndex === this.state.comparison.index
    )
      nextCurrentIndex += 1;

    addPoint(selectedIndex, theSelectors, setTheSelectors);
    updateCurrent(this.setCurrent, nextCurrentIndex, theSelectors);

    if (isComparisonsCompleted && isCurrentCompleted) {
      findTopThree(this.props.theSelectors, this.setTopThree);
      this.setState({
        completedComparisons: true,
      });
    }

    if (isCurrentCompleted) this.nextComparison(theSelectors);
  }

  private nextComparison(theSelectors: tier[]) {
    this.updateComparison(this.state.comparison.index + 1);
    updateCurrent(this.setCurrent, 0, theSelectors);
  }
}
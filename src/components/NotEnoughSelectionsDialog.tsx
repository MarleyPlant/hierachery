import {
  Dialog,
  DialogTitle,
  DialogActions,
  DialogContent,
  DialogContentText,
  Button,
} from "@material-ui/core";

export function NotEnoughSelectionsDialog(props: any) {
  return (
    <Dialog
      open={props.open}
      onClose={() => {
        props.set(true);
      }}
    >
      <DialogTitle id="alert-dialog-title">Add More Selections.</DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          You cannot compare less than <b>3</b> Selections, Please add more
          using the text field.
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button
          onClick={() => {
            props.set(false);
          }}
          color="primary"
          autoFocus
        >
          Okay
        </Button>
      </DialogActions>
    </Dialog>
  );
}

import React from "react";
import { Grid } from "@material-ui/core";

function ContainerCenter(props: any) {
  return (
    <Grid
      container
      spacing={0}
      direction="column"
      alignItems="center"
      justifyContent="center"
      style={{ minHeight: "80vh" }}
    >
      {props.children}
    </Grid>
  );
}

export default ContainerCenter;

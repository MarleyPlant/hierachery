import React from "react";
import GoogleFontLoader from "react-google-font-loader";

function FontLoader() {
  return (
    <GoogleFontLoader
      fonts={[
        {
          font: "Roboto",
          weights: [100, 200, 300, 400],
        },
      ]}
      subsets={["cyrillic-ext", "greek"]}
    />
  );
}

export default FontLoader;

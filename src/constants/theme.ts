import { createTheme } from "@material-ui/core/styles";

export var theme = createTheme({
  palette: {
    primary: {
      main: "#0b983a",
    },
    secondary: {
      main: "#e30613",
    },
  },
});
